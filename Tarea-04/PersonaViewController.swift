//
//  PersonaViewController.swift
//  Tarea-04
//
//  Created by Aldair Raul Cosetito Coral on 5/30/20.
//  Copyright © 2020 Aldair Cosetito. All rights reserved.
//

import UIKit

class PersonaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: - Constantes
    let cellId = "personaCell"
    
    //MARK: - Propiedades
    @IBOutlet weak var personasTableView: UITableView!
    let alumnos: [String] = ["Jose", "Aldair", "Joel", "Mario", "Sandra", "Jesus", "Joel", "Nelson", "Bladimir", "Eduardo"]
    
    let cursosAlumnos: [String: [String]] = ["Android": ["Jose", "Mario", "Joel", "Sandra"] , "IOS": ["Aldair", "Nelson", "Bladimir", "Eduardo"]]
    
    var secciones: [String] = Array()
    
    var totalSeccion:Int = 0
    
    //MARK: - Ciclo de vida
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        secciones = Array(cursosAlumnos.keys)
    }
    
    //MARK: -UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let seccionActual = secciones[section]
        
        if section == 0 {
            return cursosAlumnos[seccionActual]?.count ?? 0
        }else{
            return cursosAlumnos[seccionActual]!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let seccion = secciones[indexPath.section]
        let alumnosPorCurso = cursosAlumnos[seccion]
        let estudiante = alumnosPorCurso![indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = estudiante
        return cell
    }
    
    //MARK: -UITableViewDelegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return secciones[section]
        }else{
            return secciones[section]
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
