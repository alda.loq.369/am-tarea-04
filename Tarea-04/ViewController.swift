//
//  ViewController.swift
//  Tarea-04
//
//  Created by Aldair Raul Cosetito Coral on 5/27/20.
//  Copyright © 2020 Aldair Cosetito. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var paisLabel: UILabel!
    @IBOutlet weak var ciudadLabel: UILabel!
    @IBOutlet weak var pickerViewPaisCiudad: UIPickerView!
    
    var paisCiudadList: [String:[String]] = ["Argentina": ["Buenos Aires", "Mendoza"],
                                       "Brasil": ["Sao Paulo", "Brasilia", "Fortaleza"],
                                       "Bolivia": ["La Paz", "Santa Cruz"],
                                       "Chile":["Santiago", "Valparaiso", "Viña del Mar"],
                                       "Colombia":["Bogota", "Medellin", "Cali"],
                                       "Ecuador": ["Guayaquil", "Quito", "Loja"],
                                       "Paraguay":["Asuncion", "Ciudad del este"],
                                       "Peru" : ["Lima", "Arequipa", "Trujillo"],
                                       "Uruguay":["Montevideo","Punta del este", "Colonia del Sacramento"],
                                       "Venezuela":["Caracas", "Maracaibo", "Zulia"]]
    
    var listaPais:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listaPais = Array(paisCiudadList.keys).sorted()
        pickerViewPaisCiudad.dataSource = self
        pickerViewPaisCiudad.delegate = self
        
    }

    //MARK: -UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == pickerViewPaisCiudad {
            return 2
        }else{
         return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewPaisCiudad && component == 0 {
            return paisCiudadList.count
        }else{
            let indicePais = self.pickerViewPaisCiudad.selectedRow(inComponent: 0)
            let pais = listaPais[indicePais]
            let cantidadCiudades = paisCiudadList[pais]
            return cantidadCiudades?.count ?? 0
        }
    }
    
    
    //MARK: -UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerViewPaisCiudad && component == 0 {
            let pais = listaPais[row]
            paisLabel.text = pais
            return pais
        }else if pickerView == pickerViewPaisCiudad && component == 1 {
            let filaSeleccionadaComponente01 = pickerViewPaisCiudad.selectedRow(inComponent: 0)
            let pais = listaPais[filaSeleccionadaComponente01]
            
            let ciudad = paisCiudadList[pais]!
            ciudadLabel.text = ciudad[row]
            return ciudad[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerViewPaisCiudad && component == 0 {
            let pais = listaPais[row]
            paisLabel.text = pais
            self.pickerViewPaisCiudad.selectRow(0, inComponent: 1, animated: true)
            self.pickerViewPaisCiudad.reloadComponent(1)
        }else if pickerView == pickerViewPaisCiudad && component == 1{
            
            let indiceComponente01 = self.pickerViewPaisCiudad.selectedRow(inComponent: 0)
            let pais = listaPais[indiceComponente01]
            let ciudades = paisCiudadList[pais]
            
            let ciudad = ciudades?[row] ?? ""
            ciudadLabel.text = ciudad
        }
    }

}

